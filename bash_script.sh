echo 'Start AES ECB...'
openssl enc -aes-128-ecb -in lena.tiff -out aes_128_ecb.bin -pass pass:hello2017!
openssl enc -aes-128-ecb -d -in aes_128_ecb.bin -out aes_128_ecb.tiff -pass pass:hello2017!
echo "Successful. Check saved files. \n"

echo 'Start AES CBC...'
openssl enc -aes-128-cbc -in lena.tiff -out aes_128_cbc.bin -pass pass:hello2017!
openssl enc -aes-128-cbc -d -in aes_128_cbc.bin -out aes_128_cbc.tiff -pass pass:hello2017!
echo "Successful. Check saved files. \n"

echo 'Start SHA-1, SHA-256 and SHA-512...'
touch SHA-1.txt
touch SHA-256.txt
touch SHA-512.txt
openssl dgst -sha1 lena.tiff > SHA-1.txt
openssl dgst -sha256 lena.tiff > SHA-256.txt
openssl dgst -sha512 lena.tiff > SHA-512.txt
echo "Successful. Check all saved text files. \n"

echo 'Start RSA-2048...'
echo 'Generating private public key for RSA...'
openssl genrsa -out key.pem 2048
cat key.pem
openssl rsa -in key.pem -pubout -out pub-key.pem 
echo 'Generating keyfile...'
openssl rand 32 -out keyfile
cat keyfile
echo "\nEncrypting keyfile with public key in RSA..."
openssl rsautl -encrypt -in keyfile -inkey pub-key.pem -pubin -out encrypted_keyfile
openssl enc -aes-128-cbc -in lena.tiff -out encrypted_rsa_aes.bin -pass file:keyfile
openssl rsautl -decrypt -in encrypted_keyfile -inkey key.pem -out decrypted_keyfile
echo "Decrypting keyfile with public key in RSA..."
openssl enc -aes-128-cbc -d -in encrypted_rsa_aes.bin -out decrypted_rsa_aes.tiff -pass file:decrypted_keyfile 
echo "Successful. Check saved files. \n"

echo "Creating signature file. \n"
openssl dgst -ecdsa-with-SHA1 -sign key.pem lena.tiff > signature.bin
echo "Successful. Check saved files. \n"

echo "Verifying signature file. \n"
openssl dgst -ecdsa-with-SHA1 -verify pub-key.pem -signature signature.bin lena.tiff
echo "Successful. \n"

echo 'All exercises completed successfully. Check all files in this directory for the encrypted and decrypted files. '
echo 'Please refer to the documentation for more details. The Documentation is named DOCUMENTATION.docx '
